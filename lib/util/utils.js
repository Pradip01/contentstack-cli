/*!
 * contentstack-cli
 * copyright (c) Contentstack
 * MIT Licensed
 */

'use strict';

/**
 * Module Dependencies.
 */

const prompt = require('prompt'),
    pkg = require('../../package.json');

// Setting these properties customizes the prompt.
/*
 * Application defined variables
 * */
let request = require('./request'),
    config,
    api;

// Start prompt.
prompt.message = "",
    prompt.delimiter = "",
    prompt.start();

let utility_instance = null;

module.exports = (_config) => {
    config = _config;
    class Utility {
        constructor() {
            if (!utility_instance) {
                api = config.get('contentstack');
                utility_instance = this;
            }
            return utility_instance;
        }

        getEnvironment(environment) {
            return request({
                url: api.host + '/' + api.version + api.urls.environments + environment,
                headers: this.headers,
                method: "GET",
                json: true
            });
        }

        getEnvironments(headers, query) {
            return request({
                url: api.host + '/' + api.version + api.urls.environments,
                qs: query || {},
                headers: headers || this.headers,
                method: "GET",
                json: true
            });
        }

        api_key() {
            return {
                type: "string",
                name: "api_key",
                required: true,
                description: 'Enter your stack api key: ',
                before: (value) => {
                    config.set('contentstack.api_key', value);
                    return value;
                }
            }
        }

        access_token() {
            return {
                type: "string",
                name: "access_token",
                required: true,
                description: 'Enter your stack access_token: ',
                before: (value) => {
                    config.set('contentstack.access_token', value);
                    api = config.get('contentstack');
                    this.headers = {
                        api_key: config.get('contentstack.api_key'),
                        access_token: config.get('contentstack.access_token'),
                        "X-User-Agent": 'contentstack-cli/' + pkg.version
                    }
                    return value;
                }
            }
        }

        path_logs() {
            return {
                type: "string",
                name: "logs",
                description: 'Enter path for logs: ',
                before:  (value) => {
                    config.set('path.logs', value);
                    return value;
                }
            }
        }

        storage_path() {
            return {
                type: "string",
                name: "storagePath",
                description: 'Enter storage path to dump content: ',
                before: (value) => {
                    config.set('storage.base_dir', value);
                    return value;
                }
            }
        }

        type() {
            return {
                type: "string",
                name: "type",
                required: false,
                description: 'Send only content_types, only assets, or all content_types and assets for sync ?[content_types/assets/all] (default:all):',
                conform: (value) => {
                    return (value == "content_types" || value == "assets" || value == "all");
                },
                before: (value) => {
                    return (value) ? value.toLowerCase() : value;
                }
            }
        }

        content_types(opts) {
            return {
                type: "string",
                name: (opts && opts.name) ? opts.name : "content_types",
                required: false,
                description: (opts && opts.description) ? opts.description : "Enter the content types(hit return/enter for all or type \",\" comma seperated content type uids): ",
                ask: () => {
                    return (prompt && prompt.history('type').value !== 'assets');
                },
                before: (input) => {
                    if (input) {
                        return input.split(",");
                    }
                    return [];
                }
            }
        }

        environment() {
            return {
                type: "string",
                name: "environment",
                required: true,
                description: "Enter the environment: ",
                before: (input) => {
                    return input.toLowerCase();
                }
            }
        }

        language(locale) {
            let masterLocale = (locale) ? locale : 'en-us';
            return {
                type: "string",
                name: "language",
                default: masterLocale,
                required: true,
                description: "Enter the language: ",
                before: (input) => {
                    return input.split(",");
                }
            }
        }

        relative_url(lang) {
            return {
                type: "string",
                name: "relative_url",
                required: true,
                description: "Enter the Relative url of " + lang +":",
                before: (input) => {
                    return input.trim().toLowerCase();
                }
            }
        }

        custom(opts) {
            opts = opts || {};
            let _temp = {
                type: (!opts.type) ? "string" : opts.type,
                format: opts.format || "string",
                name: opts.name || "",
                required: opts.required || false,
                default: opts.default || "",
                description: opts.description || "",
                message: opts.message || "",
                before: opts.before || function (input) {
                    return input;
                },
                conform: opts.conform || function () {
                    return true;
                }
            };
            return _temp;
        }

        inputEnvironment(option) {

            // Setting these properties customizes the prompt.
            prompt.message = prompt.delimiter = "";
            prompt.start();

            return (callback) => {
                prompt.get([option], (err, result) => {
                    if (err) throw err;
                    this
                        .getEnvironment(result[option.name])
                        .then( (result) => {
                            callback(null, result);
                        })
                        .fail( (err) => {
                            callback(err);
                        });
                });
            }
        }

        inputCustom(option) {
            return (callback) => {
                prompt.get([option], (err, result) => {
                    if (err) throw err;
                    callback(null, result);
                });
            }
        }

        configFile(option, locale) {
            if (option === "languages") {
                let masterLocale = (locale) ? locale : 'en-us';
                return (callback) => {
                    let value = config.get(option);
                    if (value == null) value = masterLocale;
                    let obj = {};
                    obj['language'] = value;
                    callback(null, obj);
                }
            } else if (option === "environment") {
                return (callback) => {
                    let value = config.get(option);
                    this
                        .getEnvironment(value)
                        .then( (result) => {
                            callback(null, result);
                        })
                        .fail( (err) => {
                            callback(err);
                        });

                }
            } else {
                return (callback) => {
                    let value = config.get(option);
                    let obj = {};
                    obj[option] = value;
                    callback(null, obj);
                }
            }
        }

        matchConfirm(confirm) {
            let regExp = new RegExp('(yes|y)', 'i');
            return (confirm) ? regExp.test(confirm) : undefined;
        }
    }
    return new Utility(_config)
}
