/*!
 * contentstack-cli
 * copyright (c) Contentstack
 * MIT Licensed
 */

'use strict'

/**
 * Module Dependencies.
 */
const fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    async = require('async');

let config_instance = null;

module.exports = (config) => {
    class Config {
        constructor() {
            if (!config_instance) {
                this.config = require('./default');
                config_instance = this;
            }
            return config_instance;
        }
// Get specific value from config.
        get(key) {
            let _value = key.split('.').reduce( (prev, crnt) => {
                if (prev && prev[crnt]) return prev[crnt];
                return;
            }, this.config);
            return _value;
        }
// Get all config.
        getAll() {
            return this.config;
        }
// Set key and value in config.
        set(key, value) {

            let config = this.config;

            let list = key.split('.'),
                sub_key = {},
                parent_key = {},
                flag = true;

            if (key == list[0]) {
                config[key] = value
            }
            else {
                list.map( (val) => {
                    if (_.has(config, val) && flag) {
                        sub_key = config[val];
                        parent_key = config[val];
                        flag = false;
                    } else {
                        if (_.isEmpty(parent_key)) {
                            parent_key[val] = value
                        } else {
                            if (val == list[list.length - 1]) {
                                sub_key[val] = value;
                            } else {
                                if (sub_key[val] === undefined) {
                                    sub_key[val] = {};
                                    sub_key = sub_key[val]
                                } else {
                                    sub_key = sub_key[val]
                                }
                            }
                        }
                    }
                });
            }
        }
// Set config details in config from prompt.
        updateConfig(args, cb) {
            let _config, input, customConfigPath;
            try {
                _config = this;
                input = args;
                customConfigPath = input.config;
                if (typeof customConfigPath !== 'undefined') {
                    customConfigPath = path.resolve(input.config);
                    if (fs.existsSync(customConfigPath)) {
                        let customConfig = require(customConfigPath);
                        let objkeys = Object.keys(customConfig);
                        for (let key in objkeys) {
                            if (objkeys[key] === 'contentstack') {
                                _config.set('contentstack.api_key', customConfig[objkeys[key]].api_key);
                                _config.set('contentstack.access_token', customConfig[objkeys[key]].access_token);

                            } else {
                                _config.set(objkeys[key], customConfig[objkeys[key]]);
                            }
                        }
                    }
                    return cb(null, null);
                } else {
                    let keys = Object.keys(input);
                    let util = require('../util/utils');
                    let utility = util(_config);

                    async.eachSeries(keys, (key, _callback) => {
                        if (key && typeof input[key] === 'undefined') {
                            switch (key) {
                                case 'api_key':
                                    var _curry = utility.inputCustom(utility.api_key());
                                    _curry( (error) => {
                                        if (error)
                                            return _callback(error);
                                        return _callback();
                                    });

                                    break;
                                case
                                'access_token':
                                    var _curry = utility.inputCustom(utility.access_token());
                                    _curry( (error) => {
                                        if (error)
                                            return _callback(error);
                                        return _callback();
                                    });

                                    break;
                                case
                                'logs':
                                    var _curry = utility.inputCustom(utility.path_logs());
                                    _curry( (error) => {
                                        if (error)
                                            return _callback(error);
                                        return _callback();
                                    });

                                    break;
                                case
                                'storagePath':
                                    var _curry = utility.inputCustom(utility.storage_path());
                                    _curry( (error) => {
                                        if (error)
                                            return _callback(error);
                                        return _callback();
                                    });

                                    break;

                                case
                                'type':
                                    var _curry = utility.inputCustom(utility.type());
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            _config.set('type', _cb.type);
                                            if (_cb.type === 'assets') {
                                                delete keys['content_types'];
                                                delete keys['skip_content_types'];
                                            }
                                            return _callback();
                                        }
                                    });
                                    break;
                                case
                                'environment':
                                    var _curry = utility.inputCustom(utility.environment());
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            _config.set('environment', _cb.environment);
                                            return _callback();
                                        }
                                    });

                                    break;
                                case
                                'languages':
                                    var _curry = utility.inputCustom(utility.language());
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            let lang = _cb.language;
                                            let language = [];
                                            return async.eachSeries(lang,  (data, ___callback) => {
                                                let _lang = utility.inputCustom(utility.relative_url(data));
                                                _lang( (err, url) => {
                                                    if (err) {
                                                        return ___callback(err)
                                                    } else {
                                                        language.push({
                                                            code: data,
                                                            relative_url_prefix: url.relative_url
                                                        });
                                                        ___callback();
                                                    }
                                                })
                                            }, () => {
                                                _config.set('languages', language);
                                                return _callback();
                                            });
                                        }
                                    });

                                    break;
                                case
                                'content_types':
                                    var _curry = utility.inputCustom(utility.content_types());
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            _config.set('content_types', _cb.content_types);
                                            return _callback();
                                        }
                                    });
                                    break;
                                case
                                'skip_content_types':
                                    var _curry = utility.inputCustom(utility.content_types({
                                        name: "skip_content_types",
                                        description: "Enter the content types to be skipped(hit return/enter for none or type \",\" comma seperated content type uids): "
                                    }));
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            _config.set('skip_content_types', _cb.skip_content_types);
                                            return _callback();
                                        }
                                    });
                                    break;
                                case
                                'datetime':
                                    var _curry = utility.inputCustom(utility.custom({
                                        name: "datetime",
                                        format: "date-time",
                                        description: "Enter the date-time(ISO String Format) from where you want to synchronize your data(hit return/enter for all data): ",
                                        conform: (input) => {
                                            if (Date.parse(input) != "NaN") {
                                                return true;
                                            }
                                            return false;
                                        },
                                        before: (input) => {
                                            if (input && Date.parse(input) != "NaN") {
                                                return new Date(input).toISOString();
                                            }
                                            return input;
                                        }
                                    }));
                                    _curry( (error, _cb) => {
                                        if (error) {
                                            return _callback(error);
                                        } else {
                                            _config.set('datetime', _cb.datetime);
                                            return _callback();
                                        }
                                    });
                                    break;

                                default:
                                    _callback();
                            }
                        }
                        else {
                            if (key !== 'api_key' && key !== 'access_token') {
                                _config.set(key, input[key]);
                                return _callback();
                            } else {
                                _config.set('contentstack.' + key, input[key]);
                                return _callback();
                            }
                        }
                    }, function execError(err) {
                        if (err)
                            return cb(err);
                        return cb(null, null);
                    });
                }
            } catch (error) {
                console.error("something went wrong in config ", error);
            }
        }
    }
    return new Config(config);

}


