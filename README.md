[![Built.io Contentstack](https://contentstackdocs.built.io/static/images/logo.png)](https://www.built.io/products/contentstack/overview)

# Built.io Contentstack Command Line Interface(CLI).

## Installation
**Note: This version of Built.io Contentstack CLI can be used only for V3 stacks. Use CLI version 1.x, for V2 stacks.**

## Commands in CLI

Built.io Contentstack CLI comes with handy commands which helps to achieve the support work for the contentstack-express like publishing, unpublishing, synchronizing the data, connecting existing stacks etc.

```

	     **
	   ***
	 ****
	***** **          .----------------------------------.
	 ****  ***        |     Built.io Contentstack!       |
	   ***  ****      '----------------------------------'
	     ** *****
	        ****
	       ***
	      **
	      
Note: This version of Built.io Contentstack CLI can be used only for V3 stacks. Use CLI version 1.x, for V2 stacks.

Built.io Contentstack Command Line Interface 3.0.0

Usage: contentstack [command]

Commands:
    sync               Synchronize current published content locally
    sync-init          Synchronize all the published content locally
   
Options:
    -h, --help     output usage information
    -V, --version  output the version number

Documentation can be found at https://docs.google.com/document/d/1is5pithDVWb5R2dHjJBeoKZXZ1bCZTiSqo0k06w9rlo/edit?usp=sharing
```
### Sync

The sync command is used to synchronize all the published content on your web application.

```
$ node index.js sync           // For start current content sync
$ node index.js sync-init     // For start all published content sync
```
You'll be guided through a step-by-step procedure to synchronize all the content of the stack.

### License
Copyright � 2012-2017 [Built.io](https://www.built.io/). All Rights Reserved.
