#!/usr/bin/env node
/*!
 * contentstack-provider
 * copyright (c) Contentstack
 * MIT Licensed
 */

'use strict';

/*!
 * Module dependencies
 */
const domain = require('domain'),
    program = require('commander'),
    async = require('async'),
    pkg = require('./package.json');

let config = require('./lib/config')();


/*
 * Application defined variables
 * */

const messages = [
    '\t     \x1b[33m**'
    , '   \x1b[33m***'
    , ' \x1b[33m****'
    , '\x1b[33m***** \x1b[31m**          \x1b[36m.----------------------------------.'
    , ' \x1b[33m****  \x1b[31m***        \x1b[36m|     Built.io Contentstack!       |'
    , '   \x1b[33m***  \x1b[31m****      \x1b[36m\'----------------------------------\''
    , '     \x1b[33m** \x1b[31m*****     '
    , '        \x1b[31m****'
    , '       \x1b[31m***'
    , '      \x1b[31m**'
].join("\n\t");


function list(val) {
    return ((val && val.length) ? val.split(',').map(String) : undefined);
}

function getDefaultOptions(opts, events) {
    let opt = {};
    for (let key in events) {
        opt[key] = updateDefaultOption(opts, key);
    }
    return opt;
}

function updateDefaultOption(opts, key) {
    if (~['content_types', 'skip_content_types'].indexOf(key) && opts[key] === true) {
        opts[key] = [];
    } else if ('backup' === key && opts[key] === true) {
        opts[key] = 'yes';
    } else if ('type' === key && opts[key] === true) {
        opts[key] = 'all';
    } else if ('language' === key && opts[key] === true) {
        opts[key] = 'en-us';
    } else if ('datetime' === key && opts[key] === true) {
        opts[key] = new Date('1970').toISOString();
    }
    return opts[key];
}

function optionConversion(options) {
    let _options = {
        environment: options.env,
        languages: options.lang
    };
    options.languages = options._events.languages = true;
    options.environment = options._events.environment = _options.environment;
    delete options.env;
    delete options.lang;
    delete options._events.env;
    delete options._events.lang;
    return _options;
}
function merge(source, destination) {
    for(let key in destination) source[key] = destination[key];
    return source;
};
// printing the Built.io Contentstack Animation
console.log('\n' + messages + '\x1b[0m\n');
console.log('\x1b[31m Note: This version of Built.io Contentstack CLI can be used only for V3 stacks. Use CLI version 1.x, for V2 stacks.\x1b[0m\n');

program
    .version(pkg.version || "0.1.x");

program
    .command('sync')
    .option('-a, --api_key <api_key>', 'Enter the stack "API KEY" to connect', undefined)
    .option('-k, --access_token <access_token>', 'Enter the "Access Token" relative to "API KEY"', undefined)
    .option('-f, --config <config>', 'Enter the configFile path of which the content needs to be synchronized', undefined)
    .option('-e, --env <environment>', 'Enter the environment of which the content needs to be synchronized', undefined)
    .option('-l, --lang [language]', 'Enter the language of which the content needs to be synchronized', undefined)
    .option('-s, --server <server>', 'Enter the server name', undefined)
    .option('-h, --logs <logs>', 'Enter  the path for logs', undefined)
    .option('-o, --storagePath <storagePath>', 'Enter the path to dump content', undefined)
    .description('Synchronize the current published entries in the current application')
    .action( (options) => {
        setImmediate( () => {
            let context = domain.create();

            // error handling in domain
            context.on('error', errorHandler);

            // running the synchronization in domain
            context.run( () => {
                let _options = optionConversion(options);
                _options = merge(getDefaultOptions(options, options._events), _options);

                async.series([
                    (callback) => {
                        config.updateConfig(_options, callback);
                    }
                ], (err) => {
                    if (!err) {
                        let Provider;
                        let customConfig = config.getAll();
                        let Sync = require('contentstack-sync');
                        let sync_instance = Sync.start(customConfig);
                        Provider = require('contentstack-provider');
                        new Provider(sync_instance, customConfig);
                    }
                });
            });
        });
    });

program
    .command('sync-init')
    .option('-a, --api_key <api_key>', 'Enter the stack "API KEY" to connect', undefined)
    .option('-k, --access_token <access_token>', 'Enter the "Access Token" relative to "API KEY"', undefined)
    .option('-f, --config <config>', 'Enter the configFile path of which the content needs to be synchronized', undefined)
    .option('-e, --env <environment>', 'Enter the environment of which the content needs to be synchronized', undefined)
    .option('-s, --server <server>', 'Enter the server name', undefined)
    .option('-t, --type [type]', 'Enter a type of content to include in publishing [content_types/assets/all]', /(content_types|assets|all)/, undefined)
    .option('-l, --lang [language]', 'Enter the language of which the content needs to be synchronized', undefined)
    .option('-c, --content_types [content_types]', 'Enter the content types to be included in synchronization (comma(",") seperated)', list, undefined)
    .option('-i, --skip_content_types [skip_content_types]', 'Enter the content types to be excluded from synchronization (comma(",") seperated)', list, undefined)
    .option('-d, --datetime [datetime]', 'Enter start date in ISO String format. Content published after this date will be synchronized (skip for all content)', undefined)
    .option('-b, --backup [backup]', 'Enter backup option', /(yes|no|y|n)/i, undefined)
    .option('-h, --logs <logs>', 'Enter  the path for logs', undefined)
    .option('-o, --storagePath <storagePath>', 'Enter the path to dump content', undefined)
    .description('Synchronize the previously published entries in the current application')
    .action( (options) => {
        setImmediate( () => {
            let context = domain.create();

            // error handling in domain
            context.on('error', errorHandler);

            // running the synchronization in domain
            context.run( () => {
                let _options = optionConversion(options);
                _options = merge(getDefaultOptions(options, options._events), _options);

                async.series([
                    (callback) => {
                        config.updateConfig(_options, callback);
                    }
                ], (err) => {
                    if (!err) {
                        let Provider;
                        let customConfig = config.getAll();
                        let Sync = require('contentstack-sync');
                        let sync_instance = Sync.init(customConfig);
                        Provider = require('contentstack-provider');
                        new Provider(sync_instance, customConfig);
                    }
                });
            });
        });
    });

// parse the input arguments
program.parse(process.argv);

// show help by default if no args
if (program.args.length == 0) {
    const message = [
        'Built.io Contentstack Command Line Interface ' + pkg.version
        , '\nUsage: contentstack [command]'
        , '\nCommands:'
        , '    sync-init          Synchronize the previously published entries in the current application'
        , '    sync               Synchronize the current publish entries in the current application'
        , '\nOptions:'
        , '    -h, --help     output usage information'
        , '    -V, --version  output the version number'
        , '\nDocumentation can be found at https://contentstackdocs.built.io/'
    ].join('\n');
    console.log(message);
    process.exit(1);
}
/*
 * Error Handler to handle the domain level error
 * */
function errorHandler(err) {
    console.error(err.message);
}

process.on('uncaughtException', (err) => {
    console.error('Caught exception: ', err.message);
    process.exit(0);
});
